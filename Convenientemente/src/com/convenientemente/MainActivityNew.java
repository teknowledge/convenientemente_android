//package com.convenientemente;
//
//import java.util.Timer;
//import java.util.TimerTask;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//
//public class MainActivityNew extends Activity {
//
//	// private Button button;
//
//	private boolean isPageLoadedComplete = false;
//	private WebView webView;
//
//	private ImageView refreshBtn;
//	private LinearLayout splash_icon;
//	ProgressBar progressDialog;
//	private ImageView load_page;
//
//	boolean isTimeUP = false;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.activity_main);
//		
//		
//		
//		
//	
//		
//		Intent intent = new Intent();
//		ComponentName comp = new ComponentName("com.android.browser","com.android.browser.BrowserActivity");
//		intent.setComponent(comp);
//		intent.setAction("android.intent.action.VIEW");
//		intent.addCategory("android.intent.category.BROWSABLE");
//		Uri uri = Uri.parse("http://www.convenientemente.it/");
//		intent.setData(uri);
//		try
//		{
//		    startActivity(intent);
//		 }
//		 catch (Exception e)
//		 {
//		   e.printStackTrace();
//		 } 
//
//		//enteer git
//		// Get webview
//		webView = (WebView) findViewById(R.id.webView);
//
//		refreshBtn = (ImageView) findViewById(R.id.refreshBtn);
//
//		load_page = (ImageView) findViewById(R.id.load_page);
//
//		// startWebView("http://www.androidexample.com/media/webview/login.html");
//
//		splash_icon = (LinearLayout) findViewById(R.id.splash_icon);
//
//		refreshBtn.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				if (isConnectingToInternet()) {
//					startWebView("http://www.convenientemente.it/");
//					// showTimer();
//				} else {
//					showDialog();
//				}
//
//				refreshBtn.setVisibility(View.INVISIBLE);
//			}
//		});
//
//		load_page.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//
//				load_page.setVisibility(View.GONE);
//				if (isConnectingToInternet()) {
//
//					progressDialog = null;
//					isTimeUP = false;
//					isPageLoadedComplete = false;
//					webView.setVisibility(View.GONE);
//					startWebView("http://www.convenientemente.it/");
//					// showTimer();
//				} else {
//					showDialog();
//				}
//
//			}
//		});
//
//		Handler handle = new Handler();
//		Runnable action = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//
//				// Intent intent = new Intent(getApplicationContext(),
//				// MainActivity.class);
//				// startActivity(intent);
//				// finish();
//
//				if (isConnectingToInternet()) {
//					startWebView("http://www.convenientemente.it/");
//
//					// showTimer();
//
//				} else {
//					splash_icon.setVisibility(View.GONE);
//					showDialog();
//				}
//
//			}
//		};
//
//		handle.postDelayed(action, 1000);
//		// startWebView("http://www.convenientemente.it/");
//
//	}
//
//	public void showDialog() {
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//				MainActivityNew.this);
//
//		// set title
//		alertDialogBuilder.setTitle("No Internet Connection");
//
//		// set dialog message
//		alertDialogBuilder
//				// .setMessage("Delete the artist")
//				.setCancelable(false)
//				.setPositiveButton("Cancel",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								// if this button is clicked, close
//								// current activity
//
//								dialog.cancel();
//
//								refreshBtn.setVisibility(View.VISIBLE);
//							}
//						})
//				.setNegativeButton("Retry",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								// if this button is clicked, just close
//								// the dialog box and do nothing
//								if (isConnectingToInternet()) {
//									startWebView("http://www.convenientemente.it/");
//								} else {
//									showDialog();
//								}
//
//							}
//						});
//
//		// create alert dialog
//		AlertDialog alertDialog = alertDialogBuilder.create();
//
//		// show it
//		alertDialog.show();
//	}
//
//	@SuppressLint("SetJavaScriptEnabled")
//	private void startWebView(String url) {
//
//		// Create new webview Client to show progress dialog
//		// When opening a url or click on link
//
//		Timer myTimer = new Timer();
//		// Start this timer when you create you task
//		myTimer.schedule(new loaderTask(), 15000); // 3000 is delay in millies
//
//		webView.setWebViewClient(new WebViewClient() {
//
//			// If you will not use this method url links are opeen in new brower
//			// not in webview
//			public boolean shouldOverrideUrlLoading(WebView view, String url) {
//
//				view.loadUrl(url);
//				return true;
//			}
//
//			// Show loader on url load
//			public void onLoadResource(WebView view, String url) {
//				if (progressDialog == null) {
//					// // in standard case YourActivity.this
//					// progressDialog = new ProgressDialog(MainActivity.this);
//					// // progressDialog = (ProgressBar)
//					// // findViewById(R.id.marker_progress);
//					// // progressDialog.setMessage("Loading...");
//					//
//					// progressDialog.getWindow().setBackgroundDrawable(
//					// new ColorDrawable(
//					// android.graphics.Color.TRANSPARENT));
//					// progressDialog.setCancelable(true);
//					//
//					// // progressDialog.setIndeterminate(true);
//					//
//					// progressDialog.show();
//
//					progressDialog = (ProgressBar) findViewById(R.id.marker_progress);
//
//					progressDialog.setVisibility(View.VISIBLE);
//
//				}
//
//				if (isTimeUP) {
//					webView.stopLoading();
//					webView.setVisibility(View.GONE);
//				}
//			}
//
//			public void onPageFinished(WebView view, String url) {
//
//				// progressDialog.setVisibility(View.GONE);
//
//				// splash_icon.setVisibility(View.GONE);
//				isPageLoadedComplete = true;
//
//				try {
//					// if (progressDialog != null && progressDialog.isShowing())
//					// {
//					// progressDialog.dismiss();
//					// // // progressDialog = null;
//					// splash_icon.setVisibility(View.GONE);
//					//
//					// }
//
//					if (progressDialog.isShown()) {
//						progressDialog.setVisibility(View.GONE);
//						splash_icon.setVisibility(View.GONE);
//					}
//
//					if (!isTimeUP) {
//						webView.setVisibility(View.VISIBLE);
//					}
//				} catch (Exception exception) {
//					exception.printStackTrace();
//				}
//			}
//
//		});
//
//		// Javascript inabled on webview
//		webView.getSettings().setJavaScriptEnabled(true);
//
//		// Other webview options
//		/*
//		 * webView.getSettings().setLoadWithOverviewMode(true);
//		 * webView.getSettings().setUseWideViewPort(true);
//		 * webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//		 * webView.setScrollbarFadingEnabled(false);
//		 * webView.getSettings().setBuiltInZoomControls(true);
//		 */
//
//		/*
//		 * String summary =
//		 * "<html><body>You scored <b>192</b> points.</body></html>";
//		 * webview.loadData(summary, "text/html", null);
//		 */
//
//		// Load url in webview
//		webView.loadUrl(url);
//
//	}
//
//	// Open previous opened link from history on webview when back button
//	// pressed
//
//	@Override
//	// Detect when the back button is pressed
//	public void onBackPressed() {
//		if (webView.canGoBack()) {
//			webView.goBack();
//		} else {
//			// Let the system handle the back button
//			super.onBackPressed();
//		}
//	}
//
//	public boolean isConnectingToInternet() {
//		ConnectivityManager connectivity = (ConnectivityManager) this
//				.getSystemService(Context.CONNECTIVITY_SERVICE);
//		if (connectivity != null) {
//			NetworkInfo[] info = connectivity.getAllNetworkInfo();
//			if (info != null)
//				for (int i = 0; i < info.length; i++)
//					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
//						return true;
//					}
//
//		}
//		return false;
//	}
//
//	/**
//	 * This class is invoke when you times up.
//	 */
//	class loaderTask extends TimerTask {
//		public void run() {
//
//			runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//
//					System.out.println("Times Up");
//					if (isPageLoadedComplete) {
//						isTimeUP = false;
//					} else {
//						isTimeUP = true;
//						// if (progressDialog != null
//						// && progressDialog.isShowing()) {
//						// progressDialog.dismiss();
//						// // progressDialog = null;
//						// load_page.setVisibility(View.VISIBLE);
//						// splash_icon.setVisibility(View.GONE);
//						//
//						// }
//
//						if (progressDialog.isShown()) {
//							progressDialog.setVisibility(View.GONE);
//							splash_icon.setVisibility(View.GONE);
//							load_page.setVisibility(View.VISIBLE);
//						}
//						// show error message as per you need.
//					}
//
//				}
//			});
//
//		}
//
//	}
//}
