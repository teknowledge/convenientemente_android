package com.convenientemente;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;

public class MainActivity extends Activity {

	public String url = "http://www.convenientemente.it/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		Handler handle = new Handler();
		Runnable action = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				openUrlInBrowser();
			}
		};

		handle.postDelayed(action, 500);

	}

	

	public void openUrlInBrowser() {
		Intent intent = new Intent();
		// ComponentName comp = new ComponentName("com.android.browser",
		// "com.android.browser.BrowserActivity");
		// intent.setComponent(comp);
		intent.setAction("android.intent.action.VIEW");
		intent.addCategory("android.intent.category.BROWSABLE");
		Uri uri = Uri.parse(url);
		intent.setData(uri);
		try {
			startActivity(intent);
			finish();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
	
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
}
